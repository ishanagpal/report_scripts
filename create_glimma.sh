#!/bin/bash
#Takes argument as dataset_id folder name (eg.7209)
#It should have 'glimma' named folder inside with all the glimma plots folder inside it
#Script will check the for subfolders inside glimma folder and based on that modify the files

UPDATE_FOLDERS=$@
for folder in $UPDATE_FOLDERS
do
    echo "Entering $folder"
    echo "============================"
    cd /mnt/data/portal_data/pylons-data/prod/reports/$folder/glimma
    folder_list=()

    for i in $(ls -d */);
    do
        folder_list+=(${i%%/})

     done
    echo "Folders found - ${folder_list[@]}"
    echo "=============================================="
    
    #now iterate over all folders
    for i in "${folder_list[@]}"
	do
	     echo "started folder --> $i"
             sh /mnt/data/portal_data/pylons-data/prod/reports/scripts/modify_glimmafiles.sh $i
             sh /mnt/data/portal_data/pylons-data/prod/reports/scripts/add_s4m_and_glimma_logo.sh $i
	     echo "finished folder --> $i"
	     echo "======================"
	done
    echo "All done"
done

