#!/bin/bash

UPDATE_FOLDERS=$@

for folder in $UPDATE_FOLDERS
do
    cd $folder
    
    echo "removing css folder from '$folder'"
    rm -r css/
    if [ $? -ne 0 ]; then
      echo "Error: Failed removing css from '$folder'!"
      exit 1
    fi
    echo "remove images folder from '$folder'"
    rm -r images/
     if [ $? -ne 0 ]; then
      echo "Error: Failed removing images from '$folder'!"
      exit 1
    fi
    echo "modify volcano html for '$folder'"
    sed -i 's|css/glimma.min.css|../../glimma_file/css/glimma.min.css|g' volcano.html
    if [ $? -ne 0 ]; then
      echo "Error: Failed modifying volcano html'$folder'!"
      exit 1
    fi
    echo "modify MD html for '$folder'"
    sed -i 's|css/glimma.min.css|../../glimma_file/css/glimma.min.css|g' MD-Plot.html
    if [ $? -ne 0 ]; then
      echo "Error: Failed modifying MD plot html'$folder'!"
      exit 1
    fi
    cd ../
done
