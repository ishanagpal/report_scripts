#takes argument as dataset_id folder
#script iterates over the sub folder and modify pca.html file accordingly
#!/bin/bash

UPDATE_FOLDERS=$@
for folder in $UPDATE_FOLDERS
do
    echo "Entering $folder"
    echo "============================"
    cd /mnt/data/portal_data/pylons-data/prod/reports/$folder/pca
    folder_list=()

    for i in $(ls -d */);
    do
        folder_list+=(${i%%/})

     done
    echo "Folders found - ${folder_list[@]}"
    echo "=============================================="
    #iterating over list of pca folders

    for i in "${folder_list[@]}"
    do
        echo "Modifying $i"
        cd /mnt/data/portal_data/pylons-data/prod/reports/$folder/pca/$i
        sed -i -e '/<\/a>/a <select id="change_pca" class="button">' pca.html
        echo "creating dropdown in folder $i"
        #raise error
        if [ $? -ne 0 ]; then
            echo "Error: Failed adding dropdown to pca.html in : '$i'!"
            exit 1
        fi
        #now iterate to create dropdown values
        for j in "${folder_list[@]}"
        do
            if [ "$i" == "$j" ]
            then
                sed -i -e '/<select id="change_pca" class="button">/a <option value='${j}' selected="selected">'$j' PCA Plots</option>' pca.html
            #raise error
            if [ $? -ne 0 ]; then
                echo "Error: Failed adding dropdown to pca.html in : '$i'!"
                exit 1
            fi
	    else
		sed -i -e '/<select id="change_pca" class="button">/a <option value='${j}'>'$j' PCA Plots</option>' pca.html
            #raise error
            if [ $? -ne 0 ]; then
                echo "Error: Failed adding dropdown to pca.html in : '$i'!"
                exit 1
            fi

	    fi
        done
        sed -i -e '/<div id="graphDiv">/i </select>' pca.html
        echo "done with folder $i"
        echo "========================="
    done

    #rename folders to folder_pca
    for i in "${folder_list[@]}"
    do
        echo "changing Folder name"
        mv /mnt/data/portal_data/pylons-data/prod/reports/$folder/pca/$i /mnt/data/portal_data/pylons-data/prod/reports/$folder/pca/${i}_pca
        #raise error
        if [ $? -ne 0 ]; then
            echo "Error: Folder not Found : '$i'!"
            exit 1
        fi

    done

done
