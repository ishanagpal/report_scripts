#!/bin/bash

UPDATE_FOLDER=$@
cd $UPDATE_FOLDER
echo "Adding Stemformatics logo to Glimma reports.."
echo "---------------------------------------------"
for file in $(ls *.html)
do
    echo "Modifying file $file"
    sed -i -e '/<body>/a <a href="https://www.stemformatics.org"><image src="http://www.stemformatics.org/img/logo.gif" x="-300" y="900" transform="rotate(-90)" width="200" height="20" ></image></a>' $file
    if [ $? -ne 0 ]; then
      echo "Error: Failed adding logo to report: '$file'!"
      exit 1
    fi
    ## Add link to Glimma (OK - not from Isha)
    sed -i -e '/<\/body>/i <p>This report was generated with <a href="https://bioconductor.org/packages/release/bioc/html/Glimma.html">Glimma</a> for R</p>' $file
    if [ $? -ne 0 ]; then
      echo "Error: Failed adding Glimma link to report: '$file'!"
      exit 1
    fi
    echo "  done"
done
echo "------------------------"
echo "-Added Logo Successfully-"
echo "-------------------------"
cd ../
